package com.company;

public class Tablet {
    private String name;
    private float price;
    private int dose;

    public Tablet(String name, float price, int dose) {
        this.name = name;
        this.price = price;
        this.dose = dose;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }
}
