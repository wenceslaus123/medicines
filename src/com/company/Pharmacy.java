package com.company;

public class Pharmacy {

    private String name;
    private String address;
    private Tablet[] tablets;

    public Pharmacy(String name, String address, Tablet[] tablets) {
        this.name = name;
        this.address = address;
        this.tablets = tablets;
    }


    public String getName() {
        return name;
    }

    public Tablet getTabletByName(String name) {
        for (Tablet tablet : tablets) {
            if (tablet.getName().equalsIgnoreCase(name)) {
                return tablet;
            }
        }
        return null;
    }
}
