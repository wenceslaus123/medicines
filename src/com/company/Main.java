package com.company;

public class Main {


    public static void main(String[] args) {
        Pharmacy[] pharmacies = generate();

        System.out.println("What medicine do you need?");



        String search = "Panadol";


        for (Pharmacy pharmacy : pharmacies) {
            Tablet tablet = pharmacy.getTabletByName(search);
            if (tablet != null) {
                System.out.println(String.format("%s has tablet with price: %.2f",
                        pharmacy.getName(),
                        tablet.getPrice()));
            }
        }


        Tablet tabletWithLessPrice = null;
        String pharmacyName = null;

        for (Pharmacy pharmacy : pharmacies) {
            Tablet tablet = pharmacy.getTabletByName(search);
            if (tablet != null) {
                if (tabletWithLessPrice == null) {
                    tabletWithLessPrice = tablet;
                    pharmacyName = pharmacy.getName();
                } else if (tabletWithLessPrice.getPrice() > tablet.getPrice()) {
                    tabletWithLessPrice = tablet;
                    pharmacyName = pharmacy.getName();
                }

            }
        }


        if (tabletWithLessPrice != null) {
            System.out.println(String.format("%s has tablet with min price: %.2f",
                    pharmacyName,
                    tabletWithLessPrice.getPrice()));
        } else {
            System.out.println("Can't find tablet");
        }


    }

    private static Pharmacy[] generate() {
        Pharmacy[] pharmacies = new Pharmacy[3];

        {
            Tablet[] tablets = new Tablet[3];
            tablets[0] = new Tablet("Panadol", 23f, 2);
            tablets[1] = new Tablet("Korvalol", 45f, 4);
            tablets[1] = new Tablet("Aspirin", 10f, 3);

            pharmacies[0] = new Pharmacy("Good Heals", "Ivanova 1", tablets);
        }

        {
            Tablet[] tablets = new Tablet[3];
            tablets[0] = new Tablet("Panadol", 13f, 2);
            tablets[1] = new Tablet("Korvalol", 65f, 4);
            tablets[1] = new Tablet("Aspirin", 15f, 3);

            pharmacies[1] = new Pharmacy("Аптека Низких Цен", "Sumska 65", tablets);
        }

        {
            Tablet[] tablets = new Tablet[3];
            tablets[0] = new Tablet("Panadol", 20f, 2);
            tablets[1] = new Tablet("Korvalol", 85f, 4);
            tablets[1] = new Tablet("Aspirin", 17.7f, 3);

            pharmacies[2] = new Pharmacy("Аптека 4", "Svobody square 2", tablets);
        }
        return pharmacies;
    }
}
